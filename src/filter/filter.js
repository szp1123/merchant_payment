/**
 * Created with Webstorm 2017.2.1
 * User: xiaogu
 * Date: 2017/11/21
 * Time: 13:28
 *
 */

/*
* @param1: [number] 毫秒数
* @param2: [string] 格式化时间格式 (2017-11-21 13:35)
*
* */

export function formatDate(millisecond, fmt) {
  let date = new Date(millisecond);

  // 年份处理
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
  }

  // 其他时间处理
  let o = {
    '(m+)': date.getMonth() + 1,
    '(d+)': date.getDate(),
    '(h+)': date.getHours(),
    '(M+)': date.getMinutes(),
    '(s+)': date.getSeconds()
  };

  for (let key in o) {
    if (new RegExp(key).test(fmt)) {
      fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 2 ? ('00' + o[key]).substr((o[key] + '').length) : o[key]);
    }
  }

  return fmt;
}

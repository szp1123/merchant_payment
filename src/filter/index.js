/**
 * Created with Webstorm 2017.2.1
 * User: xiaogu
 * Date: 2017/11/21
 * Time: 13:27
 *
 */

import * as filter from './filter';

export default {
  formatDate: filter.formatDate
}

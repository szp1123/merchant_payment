// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
// 过滤器处理
import filter from './filter';

Object.keys(filter).forEach((attr) => {
  Vue.filter(attr, filter[attr]);
});
axios.defaults.baseURL= 'http://121.43.187.220/';
// axios.defaults.baseURL= '/api/';
Vue.prototype.axios = axios;
Vue.prototype.formatDate = filter.formatDate;
import {DatePicker, Select, Option, Button, Table, TableColumn, Pagination, Input, MessageBox, Message,Checkbox,Loading,Col,Row,Form,FormItem} from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

// 初始化css
import '../static/css/base.css'
// 使用elementUI
Vue.use(DatePicker);
Vue.use(Select);
Vue.use(Option);
Vue.use(Button);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Pagination);
Vue.use(Input);
Vue.use(Checkbox);
Vue.use(Loading);
Vue.use(Col);
Vue.use(Row);
Vue.use(Form);
Vue.use(FormItem);

Vue.config.productionTip = false;

Vue.prototype.ElementUIMsg = Message;
Vue.prototype.ElementUIMsgBox = MessageBox;

router.beforeEach((to, from, next) => {
  if (to.path !== '/login' && !to.path.match(/404/)) {
    if (!localStorage['token']) {
      router.replace({
        path: '/login'
      });

      return false;
    }
    document.title = to.meta.title;
    next();
    return false;
  }
  document.title = to.meta.title;
  next();
  return false;
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});

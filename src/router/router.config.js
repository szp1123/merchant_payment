
import Login from '../components/Login/Login';
import Index from '../components/Index/Index';
import Pay from '../components/Pay/Pay';
import PayRoll from '../components/PayRoll/PayRoll';
import User from '../components/User/User';
import N404 from '../components/404/404';
export default {
  routes:[
    {
      path:'/',
      redirect:'/login'
    },
    // 登录
    {
      path:'/login',
      name:'login',
      component:Login,
      meta:{
        title:'登录'
      }
    },
    // 主页
    {
      path:'/index',
      name: 'index',
      component: Index,
      children: [
        // 代付
        {
          path: '/index/pay',
          name: 'pay',
          component: Pay,
          meta:{
            title:'支付中心'
          }
        },
        // 支付
        {
          path: '/index/payroll',
          name: 'payroll',
          component: PayRoll,
          meta:{
            title:'代付中心'
          }
        },
        // 用户中心
        {
          path: '/index/user',
          name: 'user',
          component: User,
          meta:{
            title:'用户中心'
          }
        },
        {
          path: '/index',
          redirect: '/index/pay',
        }
      ]
    },
    {
      path: '/404',
      component: N404,
      meta: {
        title: '404'
      }
    },
    {
      path: '*',
      redirect: '/404'
    }
  ],
  base: 'public/greenroom/', // 测试 加的路径前缀
  linkActiveClass:'active'
}
